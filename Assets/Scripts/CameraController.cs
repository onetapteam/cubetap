﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    Transform playerTransform;

    Transform mTransform;

    void Start()
    {
        mTransform = this.GetComponent<Transform>();
    }

	void Update ()
    {
        mTransform.position = new Vector3(playerTransform.position.x, mTransform.position.y, mTransform.position.z);
	}
}
