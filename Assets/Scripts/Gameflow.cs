﻿using UnityEngine;
using UnityEngine.Events;

public static class Gameflow
{
    public static UnityAction OnResume;

    public static void GameOver(int points)
    {
        
    }

    public static void Pause()
    {

    }

    public static int BestScore
    {
        get
        {
            return Random.Range(0, 100);
        }
    }
}
