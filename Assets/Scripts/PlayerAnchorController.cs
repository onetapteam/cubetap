﻿using UnityEngine;
using System.Collections;

public class PlayerAnchorController : MonoBehaviour
{
    [SerializeField]
    Transform anchorTransform;
    [SerializeField]
    Transform castPosition;
    [SerializeField]
    DistanceJoint2D distanceJoint;

    public void RelocateAnchor()
    {
        var raycastMask = 1 << 8;
        raycastMask = ~raycastMask;

        var raycastHit = Physics2D.Raycast(castPosition.position, new Vector2(.15f, 1f), 100, raycastMask);

        anchorTransform.position = raycastHit.point;
    }
}
