﻿using UnityEngine;
using System.Collections;
using System;

public class InputReciever : MonoBehaviour
{
    [SerializeField]
    PlayerAnchorController anchorController;

    void Update()
    {
        RecieveInputs();
    }

    private void RecieveInputs()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Debug.Log("Bang!");
                anchorController.RelocateAnchor();
            }
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {

            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Bang!");
            anchorController.RelocateAnchor();
        }
        
    }
}
